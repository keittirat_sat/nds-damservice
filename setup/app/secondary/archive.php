<?php
return array(
    'path'             => public_path('archive'),
    'secondary_server' => null,
    'is_master'        => false
);
