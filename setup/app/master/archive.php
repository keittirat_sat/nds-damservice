<?php
return array(
    'path'             => public_path('archive'),
    'secondary_server' => 'archive.damlog.com',
    'is_master'        => true
);
