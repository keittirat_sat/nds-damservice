<?php

namespace App\Console\Commands;

use Illuminate\Console\Command,
    Illuminate\Support\Facades\DB,
    App\Model\SyncControl,
    App\Repositories\AlertRepository,
    App\Libraries\NdsDB;

class HealthCheck extends Command
{

    private $alert_repo;
    private $ndsdb;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check All Sync status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->alert_repo = new AlertRepository();
        $this->ndsdb      = new NdsDB();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('app.save_to_db')) {
            $data = SyncControl::all()->toArray();
        } else {
            $data = $this->ndsdb->readSyncControl();
        }

        $current_unix = time();
        $current_time = date('Y-m-d H:i:s');

        $this->line('[' . $current_time . '] start health check');

        $project = [
            'seismic'  => [],
            'pressure' => []
        ];

        /**
         * Grouping
         */
        foreach ($data as $site_project) {
            if (stristr($site_project['site_id'], 'pressure')) {
                $project['pressure'][$site_project['site_id']] = [
                    'updated_at'         => strtotime($site_project['updated_at']),
                    'latest_record_time' => $site_project['latest_record_time']
                ];
            } else {
                $project['seismic'][$site_project['site_id']][$site_project['station']] = [
                    'updated_at'         => strtotime($site_project['updated_at']),
                    'latest_record_time' => $site_project['latest_record_time']
                ];
            }
        }

        /**
         * Check by Group
         */
        foreach ($project['pressure'] as $site_id => $rec) {
            $gap_blackout = config('sensor.' . $site_id . '.no_update_interval');
            $is_online    = $current_unix - $rec['updated_at'] >= $gap_blackout ? false : true;

            if (!$is_online) {
                $temp = [
                    'full_name'        => config('sensor.' . $site_id . '.site_name'),
                    'time'             => $current_time,
                    'last_updated'     => date('Y-m-d H:i:s', $rec['updated_at']),
                    'last_record_time' => $rec['latest_record_time']
                ];

                $recv_name  = config("sensor.{$site_id}.recv_name");
                $recv_email = config("sensor.{$site_id}.error_report");
                $recv_email = implode(',', $recv_email);
                $subject    = '[' . config('sensor.' . $site_id . '.site_name') . '] ' . config('sensor.' . $site_id . '.email_warning_subject');
                $ch         = config('sensor.' . $site_id . '.email_channel');
                $html       = (string) view('emails.warning.pressure', $temp);

                $this->alert_repo->sendEmail($subject, $html, $recv_email, $recv_name, $ch);
                $this->line('[' . $current_time . '] send warning eror email for ' . $site_id);
            } else {
                $this->line('[' . $current_time . '] ' . $site_id . ' is OK');
            }
        }

        foreach ($project['seismic'] as $site_id => $all_station) {
            $gap_blackout = config('sensor.' . $site_id . '.no_update_interval');

            $seismic_online  = [];
            $seismic_offline = [];

            foreach ($all_station as $station => $rec) {
                $status = $current_unix - $rec['updated_at'] >= $gap_blackout ? 'offline' : 'online';
                $str    = ' สถานี ' . strtoupper($station) . ': Last connection: ' . date('Y-m-d H:i:s', $rec['updated_at']) . ', Last Data: ' . $rec['latest_record_time'];
                if ('online' == $status) {
                    $seismic_online[] = $str;
                } else {
                    $seismic_offline[] = $str;
                }
            }

            if (!empty($seismic_offline)) {
                $temp = [
                    'full_name' => config('sensor.' . $site_id . '.site_name'),
                    'time'      => $current_time,
                    'online'    => $seismic_online,
                    'offline'   => $seismic_offline
                ];

                $recv_name  = config("sensor.{$site_id}.recv_name");
                $recv_email = config("sensor.{$site_id}.error_report");
                $recv_email = implode(',', $recv_email);
                $subject    = '[' . config('sensor.' . $site_id . '.site_name') . '] ' . config('sensor.' . $site_id . '.email_warning_subject');
                $ch         = config('sensor.' . $site_id . '.email_channel');
                $html       = (string) view('emails.warning.seismic', $temp);

                $this->alert_repo->sendEmail($subject, $html, $recv_email, $recv_name, $ch);
                $this->line('[' . $current_time . '] send warning eror email for ' . $site_id);
            } else {
                $this->line('[' . $current_time . '] ' . $site_id . ' is OK');
            }
        }
    }
}
