<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Libraries;

/**
 * Description of RespClient
 *
 * @author keitt
 */
class RespClient
{

    /**
     * 
     * @param string $url
     * @param array $body
     * @return string
     */
    public function postJson($url, $body, $async = false)
    {
        $curl = curl_init();

        $option = [
            CURLOPT_URL           => $url,
            CURLOPT_FRESH_CONNECT => true,
            CURLOPT_ENCODING      => "utf",
            CURLOPT_MAXREDIRS     => 10,
            CURLOPT_HTTP_VERSION  => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS    => json_encode($body),
            CURLOPT_HTTPHEADER    => [
                "cache-control: no-cache",
                "content-type: application/json"
            ]
        ];

        if ($async) {
            $option[CURLOPT_TIMEOUT] = 1;
            $option[CURLOPT_RETURNTRANSFER] = false;
            $option[CURLOPT_FORBID_REUSE] = true;
            $option[CURLOPT_CONNECTTIMEOUT] = 1;
            $option[CURLOPT_DNS_CACHE_TIMEOUT] = true;
        } else {
            $option[CURLOPT_TIMEOUT]        = 120;
            $option[CURLOPT_RETURNTRANSFER] = true;
        }

        curl_setopt_array($curl, $option);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}
