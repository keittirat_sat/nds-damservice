<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Libraries;
use App\Libraries\NdsDB;

/**
 * Description of QueueDataAdapter
 *
 * @author keittirat.sat
 */
class QueueDataAdapter
{
    
    private $nds_db;
    public function __construct()
    {
        $this->nds_db = new NdsDB();
    }
    
    /**
     * Convert Seismic Data
     * @param array $data
     * @return array
     */
    public function seismicConverter($data)
    {
        $data_seismic = [];

        foreach ($data as $unixtime => $each_record) {
            foreach ($each_record as $station => $all_channel) {
                $data_seismic[] = [
                    'station' => $station,
                    'unix'    => $this->nds_db->reCheckDate($unixtime),
                    'ch0'     => $all_channel['ch0'],
                    'ch1'     => $all_channel['ch1'],
                    'ch2'     => $all_channel['ch2'],
                    'ch0_max' => max(array_map('abs', $all_channel['ch0'])),
                    'ch1_max' => max(array_map('abs', $all_channel['ch1'])),
                    'ch2_max' => max(array_map('abs', $all_channel['ch2']))
                ];
            }
        }

        return $data_seismic;
    }

    /**
     * Convert Pressure Data
     * @param array $data
     * @return array
     */
    public function pressureConverter($data)
    {
        $data_pressure = [];

        foreach ($data as $unixtime => $each_record) {
            $temp         = $each_record;
            $temp['unix'] = $this->nds_db->reCheckDate($unixtime);

            $data_pressure[] = $temp;
        }

        return $data_pressure;
    }
}
