<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Libraries;

/**
 * Description of DataMonitor
 *
 * @author keittirat.sat
 */
class DataMonitor
{

    private $site_id;
    private $config;

    /**
     * 
     * @param string $site_id
     */
    public function __construct($site_id)
    {
        $this->site_id = $site_id;
        $this->config  = config('sensor.' . $site_id);
    }

    /**
     * Monitoring Accelerate
     * @param array $data
     */
    public function seismicWave($data)
    {
        $data_event    = [];
        $warning_level = $this->config['max_seismic'];
        $alert_level   = $this->config['alert_seismic'];
        $accelerometer = $this->config['accelerometer'];

        /**
         * Debug
         */
//        $warning_level = 0;
//        $alert_level   = 0;

        $alert_data = [];

        $max_seismic_value = 0;
        $max_seismic_unix  = 0;

        foreach ($data as $each_rec) {
            if (in_array($each_rec['station'], $accelerometer)) {
                $temp = max([$each_rec['ch0_max'], $each_rec['ch1_max'], $each_rec['ch2_max']]);

                /**
                * Acc more than minimum
                */
                if ($temp >= $warning_level) {
                    if (empty($data_event[$each_rec['unix']])) {
                        $data_event[$each_rec['unix']]['unix'] = $each_rec['unix'];
                        $data_event[$each_rec['unix']]['data'] = [];
                    }

                    $data_event[$each_rec['unix']]['data'][] = [
                        'station' => $each_rec['station'],
                        'g'       => $temp,
                        'alert'   => $alert_level <= $temp ? true : false
                    ];

                    if ($temp >= $alert_level && $temp > $max_seismic_value) {
                        $max_seismic_value = $temp;
                        $max_seismic_unix  = $each_rec['unix'];

                        $alert_data = [
                            'station' => $each_rec['station'],
                            'unix'    => $max_seismic_unix,
                            'g'       => $max_seismic_value
                        ];
                    }
                }
            }
        }

        return [
            'alert_level'   => $alert_data,
            'warning_level' => array_values($data_event)
        ];
    }
}
