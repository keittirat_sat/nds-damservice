<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Libraries;

/**
 * Description of NdsDB
 *
 * @author keittirat.sat
 */
class NdsDB
{

    /**
     * makeDir($site_id, $unix)
     * @param type $site_id
     * @param type $unix
     * @return string
     */
    public function makeDir($site_id, $unix)
    {
        $is_pressure = preg_match("/pressure/", $site_id);

        $path = config('archive.path') . '/' . $site_id . '/' . date('Y', $unix) . '/' . date('n', $unix);
        if (!$is_pressure) {
            $path .= '/' . date('j', $unix);
        }

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }

    public function reCheckDate($unix)
    {
//        $year = date('Y', $unix);
//        if ($year < 2000) {
//            /**
//             * year += 543
//             */
//            $str = ($year + 543) . '-' . date('m-d H:i:s', $unix);
//            $unix = strtotime($str);
//        }
        return $unix;
    }

    /**
     * encodeData($data)
     * @param type $data
     * @return type
     */
    public function encodeData($data)
    {
        return base64_encode(gzcompress($data));
    }

    /**
     * decodeData($data)
     * @param type $data
     * @return type
     */
    public function decodeData($data)
    {
        return gzuncompress(base64_decode($data));
    }

    public function readSyncControl()
    {
        $path = config('archive.path') . '/sync_control.json';
        if (!file_exists($path)) {
            file_put_contents($path, "[]");
        }

        $dat = file_get_contents($path);
        return json_decode($dat, true);
    }

    public function saveSyncControl($data)
    {
        $path = config('archive.path') . '/sync_control.json';
        if (is_array($data)) {
            $data = json_encode($data);
        }
        file_put_contents($path, $data);
    }

    /**
     * savePressure($site_id, $unix, $data)
     * @param type $site_id
     * @param type $unix
     * @param type $data
     */
    public function savePressure($site_id, $unix, $data, $overwrite = false)
    {
        if (!is_string($data)) {
            $data = json_encode($data);
        }

        $line = $unix . ',' . $this->encodeData($data) . PHP_EOL;

        $path = $this->makeDir($site_id, $unix);
        $path .= '/' . date('j', $unix) . '.json';

        if ($overwrite) {
            file_put_contents($path, $line);
        } else {
            file_put_contents($path, $line, FILE_APPEND);
        }
    }

    /**
     * saveSeismic($site_id, $unix, $station, $data)
     * @param type $site_id
     * @param type $unix
     * @param type $station
     * @param type $data
     */
    public function saveSeismic($site_id, $unix, $station, $data, $overwrite = false)
    {
        if (!is_string($data)) {
            $data = json_encode($data);
        }

        $line = $unix . ',' . $this->encodeData($data) . PHP_EOL;
        $path = $this->makeDir($site_id, $unix);
        $path .= '/' . $station . '.json';

        if ($overwrite) {
            file_put_contents($path, $line);
        } else {
            file_put_contents($path, $line, FILE_APPEND);
        }
    }

    /**
     * saveSeismicEvent($site_id, $unix, $data)
     * @param type $site_id
     * @param type $unix
     * @param type $data
     */
    public function saveSeismicEvent($site_id, $unix, $data)
    {
        if (!is_string($data)) {
            $data = json_encode($data);
        }

        $line = $unix . ',' . $this->encodeData($data) . PHP_EOL;
        $path = config('archive.path') . '/' . $site_id;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $path .= '/event.json';
        file_put_contents($path, $line, FILE_APPEND);
    }

    /**
     * convertDatData($all_data)
     * @param array $all_data
     * @return array
     */
    public function convertDatData($all_data)
    {
        $db  = [];
        $all = explode(PHP_EOL, $all_data);
        foreach ($all as $each_line) {
            if (!empty($each_line)) {
                $temp         = explode(",", $each_line, 2);
                $db[$temp[0]] = json_decode($this->decodeData($temp[1]), true);
            }
        }
        ksort($db);
        return $db;
    }

    /**
     * readPressure($site_id, $unix_start, $unix_end)
     * @param string $site_id
     * @param int $unix_start
     * @param int $unix_end
     * @return array
     */
    public function readPressure($site_id, $unix_start, $unix_end)
    {
        $final_data = [];
        $path       = $this->makeDir($site_id, $unix_start);
        $path .= '/' . date('j', $unix_start) . '.json';

        if (!file_exists($path)) {
            return [];
        }

        $all_data = file_get_contents($path);
        $all_data = $this->convertDatData($all_data);

        foreach ($all_data as $unix => $each_rec) {
            if ($unix >= $unix_start && $unix <= $unix_end) {
                $final_data[$unix]         = $each_rec;
                $final_data[$unix]['unix'] = $unix;
            }
        }

        return $final_data;
    }

    /**
     * readSeismic($site_id, $station, $unix_start, $unix_end)
     * @param string $site_id
     * @param string $station
     * @param int $unix_start
     * @param int $unix_end
     * @return array
     */
    public function readSeismic($site_id, $station, $unix_start, $unix_end)
    {
        $final_data     = [];
        $path           = $this->makeDir($site_id, $unix_start);
        $case_not_found = $path . '/' . strtolower($station) . '.json';
        $path .= '/' . $station . '.json';

        if (!file_exists($path)) {
            if (!file_exists($case_not_found)) {
                return [];
            } else {
                $path = $case_not_found;
            }
        }

        $all_data = file_get_contents($path);
        $all_data = $this->convertDatData($all_data);

        foreach ($all_data as $unix => $each_rec) {
            if ($unix >= $unix_start && $unix <= $unix_end) {
                $final_data[$unix]            = $each_rec;
                $final_data[$unix]['unix']    = $unix;
                $final_data[$unix]['station'] = $station;
            }
        }

        return $final_data;
    }

    /**
     * readSeismicEvent($site_id, $page, $limit)
     * @param string $site_id
     * @param int $page
     * @param int $limit [Optional default is 20]
     * @return array
     */
    public function readSeismicEvent($site_id, $page, $limit = 20)
    {
        $final_data = [];
        $path       = config('archive.path') . '/' . $site_id . '/event.json';
        $all_data   = file_get_contents($path);
        $all_data   = $this->convertDatData($all_data);
        $all_data   = array_reverse($all_data, true);

        /**
         * Convert as Mongo Data
         */
        $final_data['total']        = count($all_data);
        $final_data['per_page']     = $limit;
        $final_data['current_page'] = $page;
        $final_data['last_page']    = ceil($final_data['total'] / $limit);
        $final_data['form']         = (($page - 1) * $limit) + 1;


        /**
         * Slice as paginate
         */
        $all_data = array_slice($all_data, (($page - 1) * $limit), $limit, true);

        /**
         * Transform Dataset
         */
        foreach ($all_data as $unix => &$each_rec) {
            $temp['unix'] = $unix;
            $temp['data'] = $each_rec;
            $each_rec     = $temp;
        }

        $final_data['to']   = (($page - 1) * $limit) + count($all_data);
        $final_data['data'] = $all_data;

        return $final_data;
    }
}
