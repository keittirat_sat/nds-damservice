<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input,
    App\Repositories\AlertRepository,
    Illuminate\Support\Facades\DB,
    App\Libraries\NdsDB;

/**
 * Description of ReportController
 *
 * @author keittirat.sat
 */
class ReportController extends Controller
{

    public function getStatus()
    {
        $nds_db = new NdsDB();
        $data   = $nds_db->readSyncControl();
        $resp   = array(
            'seismic'  => array(),
            'pressure' => array()
        );

        foreach ($data as $each_sensor) {
            if (preg_match("/seismic/", $each_sensor['site_id'])) {
                if (!isset($resp['seismic'][$each_sensor['site_id']])) {
                    $resp['seismic'][$each_sensor['site_id']] = array();
                }

                $resp['seismic'][$each_sensor['site_id']][] = array(
                    'station'            => $each_sensor['station'],
                    'updated_at'         => $each_sensor['updated_at'],
                    'latest_record_time' => $each_sensor['latest_record_time'],
                );
            } else {
                $resp['pressure'][$each_sensor['site_id']] = array(
                    'station'            => $each_sensor['site_id'],
                    'updated_at'         => $each_sensor['updated_at'],
                    'latest_record_time' => $each_sensor['latest_record_time'],
                );
            }
        }

        $now  = strtotime('now');
        $html = "<h1>All Station Status</h1>";
//        $html .= "<h2>Readable Info</h2>";
        foreach ($resp['pressure'] as $each_site) {
            $current_updated = strtotime($each_site['updated_at']);
            $is_online       = $now - $current_updated < 900 ? "<span style='color:green'>online</span>" : "<span style='color:red'>offline</span>";
            $html .= "<h3>Pressure: " . $each_site['station'] . " : " . $is_online . " / " . $each_site['latest_record_time'] . "</h3>";
        }

        foreach ($resp['seismic'] as $station => $each_site) {
            $html .= "<h3>Seismic: " . $station . "</h3>";
            $html .= '<ul>';
            foreach ($each_site as $info) {
                $current_updated = strtotime($info['updated_at']);
                $is_online       = $now - $current_updated < 900 ? "<span style='color:green'>online</span>" : "<span style='color:red'>offline</span>";
                $html .= '<li>' . $info['station'] . ': ' . $is_online . ' / ' . $info['latest_record_time'] . '</li>';
            }
            $html .= '</ul>';
        }

//        $html .= "<h2>Raws Info</h2>";
        $html .= "<pre style='display:none;'>" . print_r($resp, true) . "</pre>";
        return $html;
    }

    public function postCurrentEvent()
    {
        $input       = Input::all();
        $site_id     = array_get($input, 'site_id');
        $chart       = array_get($input, 'type');
        $time        = array_get($input, 'time');
        $mid         = array_get($input, 'mid');
        $unix        = strtotime($time);
        $server_time = date('Y-m-d H:i:s');
        $line_img    = [];

        $s = new AlertRepository();

        $path_sensor    = 'sensor.' . $site_id . '_seismic';
        $dam_full_name  = config($path_sensor . '.site_name');
        $accelerometer  = config($path_sensor . '.accelerometer');
        $max_accelerate = config($path_sensor . '.max_accelerate');
//        $email_list     = config($path_sensor . '.alert_email_report');
        $email_list     = 'keittirat@trycatch.in.th';

        $all_accelerate = [];

        $line_img[] = $chart['pressure'];
        foreach ($chart['seismic'] as $each_img) {
            $line_img[] = $each_img;
        }

        if ($mid) {
            $mid = is_array($mid) ? $mid : [$mid];
            $s->sendImgLineDirect($mid, $line_img);
            return ['code' => 200, 'message' => 'direct', 'reply' => $mid];
        } else {

            /**
             * Get Seismic Wave
             */
            $db_collection   = date('Ymd', $unix) . '_' . $site_id . '_seismic';
            $current_seismic = DB::connection('mongodb')->collection($db_collection)->where('unix', '=', $unix)->get();

            $html = "<h4>รายงานแจ้งผลกระทบของ {$dam_full_name}</h4>";
            $html .= "<p>เกิดแผ่นดินไหวเมื่อเวลา {$time} อุปกรณ์สามารถวัดค่าแรงดันน้ำภายในอ่าง<br><img src='{$chart['pressure']}' style='max-width:100%;'></p>";
            foreach ($current_seismic as $each_station) {
                $html .= "<p><b>สถานีตรวจวัดแผ่นดินไหว {$each_station['station']}</b><br>";
                $html .= "<img src='{$chart['seismic'][$each_station['station']]}' style='max-width:100%;'>";
                $html .= "<ul>";
                $html .= "<li>{$each_station['station']}::ch0 = {$each_station['ch0_max']}g</li>";
                $html .= "<li>{$each_station['station']}::ch1 = {$each_station['ch1_max']}g</li>";
                $html .= "<li>{$each_station['station']}::ch2 = {$each_station['ch2_max']}g</li>";
                $html .= "</ul>";
                $html .= "</p>";

                if (in_array($each_station['station'], $accelerometer)) {

                    $all_accelerate[] = $each_station['ch0_max'];
                    $all_accelerate[] = $each_station['ch1_max'];
                    $all_accelerate[] = $each_station['ch2_max'];
                }
            }

            $max_current_seismic = max($all_accelerate);
            $max_seismic_effect  = $max_current_seismic * 100 / $max_accelerate;
            $html .= "<p>โดย {$dam_full_name} วัดได้สูงสุด <strong style='color:red;'>" . number_format($max_current_seismic, 8) . "g</strong> ผลกระทบต่อตัวเขื่อน <strong style='color:red;'>" . number_format($max_seismic_effect, 4) . "%</strong>  ค่าสูงสุดที่เขื่อนรับได้โดยไม่เกิดความเสียหาย {$max_accelerate}g</p>";
            $html .= '<p>Report Generated by TryCatch | NDS @ ' . $server_time . '</p>';

            $s->sendEmail("รายงานแจ้งผลกระทบของ {$dam_full_name} เหตุการณ์เสี่ยงเกิดแผ่นดินไหวที่เวลา {$time}", $html, $email_list, 'Royal Irrigation Department', 'irr-export');

            /**
             * Line
             * - temporary disable
             */
//            $text = "รายงานผลกระทบ เกิดแผ่นดินไหวเมื่อ เวลา {$time} {$dam_full_name} วัดได้ " . number_format($max_current_seismic, 8) . "g ผลกระทบต่อตัวเขื่อน " . number_format($max_seismic_effect, 4) . "% ซึ่งค่าสูงสุดที่เขื่อนรับได้ โดยไม่เกิดความเสียหาย คือ {$max_accelerate}g";
//            $s->sendLineWithImg($text, $line_img, 'irr_export');

            return ['code' => 200, 'result' => $current_seismic];
        }
    }
}
