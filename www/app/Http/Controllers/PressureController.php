<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input,
    App\Repositories\PressureRepository,
    App\Repositories\SecondaryHost,
    Illuminate\Http\Response,
    App\Model\SyncControl,
    Illuminate\Support\Facades\DB,
    App\Libraries\QueueDataAdapter,
    App\Libraries\NdsDB,
    Illuminate\Http\Request,
    App\Libraries\RespClient;

/**
 * Description of PressureController
 *
 * @author keitt
 */
class PressureController extends Controller
{

    private $pressure_repo;
    private $data_convert;
    private $nds_db;
    private $request;
    private $client;

    public function __construct(Request $request)
    {
        $this->pressure_repo = new PressureRepository();
        $this->data_convert  = new QueueDataAdapter();
        $this->nds_db        = new NdsDB();
        $this->request       = $request;
        $this->client        = new RespClient();
    }

    public function getIndex()
    {
        $site_id = Input::get('site_id', NULL);
        $start   = Input::get('start', date('Y-m-d') . ' 00:00:00');
        $end     = Input::get('end', date('Y-m-d') . ' 23:59:59');

        $unix_start = strtotime($start);
        $unix_end   = strtotime($end);

        if (empty($site_id) || $unix_start >= $unix_end) {
            $resp = [
                'code'    => 500,
                'message' => 'Unauthorize access'
            ];
            return Response::create($resp);
        }

        $data         = [];
        $data_reorder = [];

        if (config('app.read_from_file')) {
            /**
             * Get Data from file
             */
            $collected_data = $this->nds_db->readPressure($site_id, $unix_start, $unix_end);
        } else {
            /**
             * Get Data from DB
             */
            $db_collection  = date('Ymd', $unix_start) . '_' . $site_id;
            $collected_data = DB::connection('mongodb')
                ->collection($db_collection)
                ->whereBetween('unix', [$unix_start, $unix_end])
                ->orderBy('unix', 'asc')
                ->get();
        }

        /**
         * Default Data
         */
        $resp = array(
            'code'   => 200,
            'result' => array(
                'start'  => $start,
                'end'    => $end,
                'stream' => array()
            )
        );

        if (!empty($collected_data)) {
            foreach ($collected_data as $each_rec) {
                unset($each_rec['_id']);
                $each_rec['timestamp']           = date('Y-n-d H:i:s', $each_rec['unix']);
                $data_reorder[$each_rec['unix']] = $each_rec;
            }

            $resp['result']['stream'] = $data_reorder;
        } else {
            /**
             * Get Data from secondary server
             */
            $is_master = config('archive.is_master');
            if ($is_master == true) {
                $path      = 'pressure';
                $all_input = Input::all();
                $raws_json = SecondaryHost::callBackupData($path, $all_input);
                if ($raws_json !== false) {
                    $resp = json_decode($raws_json, true);
                }
            }
        }

        return Response::create($resp);
    }

    public function getSync()
    {
        $site_id = Input::get('site_id', null);

        if (empty($site_id)) {
            return Response::create(['code' => 500, 'messages' => 'Site Ids undefined'], 500);
        }

        if (config('app.read_from_file')) {
            $data     = [];
            $ref_data = $this->nds_db->readSyncControl();
            foreach ($ref_data as $each_rec) {
                if ($each_rec['site_id'] == $site_id) {
                    $data = $each_rec;
                }
            }
        } else {
            $data = SyncControl::where('site_id', '=', $site_id)->first()->toArray();
        }

        if ($data) {
            $time = strtotime($data['latest_record_time']);
        } else {
            $time = time();
        }

        /**
         * Add 1 secord prevent data duplicate
         */
        $time++;

        $resp = [
            'code'  => 200,
            'start' => date('Y-m-d', $time) . 'T' . date('H:i:s', $time),
            'end'   => date('Y-m-d', ($time + 7199)) . 'T' . date('H:i:s', ($time + 7199))
        ];

        return Response::create($resp);
    }

    public function postSync()
    {
        $site_id              = Input::get('site_id', NULL);
        $data                 = Input::get('data', NULL);
        $encode               = Input::get('encode', 0);
        $disable_sync_control = Input::get('disable_sync_control', 0);

        if (empty($site_id) || empty($data)) {
            return Response::create(['code' => 500, 'messages' => 'Data not found'], 500);
        }

        /**
         * Check Data type
         */
        if ($encode != 0) {
            $data = rawurldecode($data);
            $data = base64_decode($data);
            $data = gzdecode($data);
        }

        $data = json_decode($data, true);

        /**
         * Process Raws Data
         */
        $save_mongo    = $this->pressure_repo->convertToMongo($site_id, $data);
        $data_pressure = $this->data_convert->pressureConverter($save_mongo['data']);

        /**
         * Push into queue
         */
        $chunk = array_chunk($data_pressure, 144);
        foreach ($chunk as $data_set) {
            $enqueue['site_id']              = $site_id;
            $enqueue['timezone']             = 'Asia/Bangkok';
            $enqueue['data']                 = $data_set;
            $enqueue['disable_sync_control'] = $disable_sync_control;

            $job = (new \App\Jobs\SavePressure($enqueue));
            $this->dispatch($job);
        }

        $last_sync = last($data_pressure);

        /**
         * Push Raws
         */
        $push_list = config('pushlist.raws.pressure');
        if (!empty($push_list)) {
            foreach ($push_list as $service_url) {
                try {
                    $this->client->postJson($service_url, Input::all(), true);
                } catch (Exception $e) {
                    
                }
            }
        }

        return Response::create([
                'code'        => 200,
                'synced'      => count($data),
                'last_record' => date('Y-m-d H:i:s', $last_sync['unix']),
                'fwd'         => $push_list
        ]);
    }

    public function getStatus()
    {
        $site_id = Input::get('site_id', null);

        if (empty($site_id)) {
            $resp = [
                'code'    => 500,
                'message' => 'Unauthorize access'
            ];

            return Response::create($resp, 500);
        }

        if (config('app.read_from_file')) {
            $data     = [];
            $ref_data = $this->nds_db->readSyncControl();
            foreach ($ref_data as $each_rec) {
                if ($each_rec['site_id'] == $site_id) {
                    $data = $each_rec;
                }
            }
        } else {
            $data = SyncControl::where('site_id', '=', $site_id)->first()->toArray();
        }

        if (empty($data)) {
            $resp = [
                'code'    => 500,
                'message' => 'Unauthorize access #2'
            ];

            return Response::create($resp, 500);
        }

        $current_unix = time();
        $updated_unix = strtotime($data['updated_at']);

        $maximum_time_diff = config('sensor/' . $site_id . '.no_update_interval');
        $current_time_diff = $current_unix - $updated_unix;
        $is_online         = $maximum_time_diff <= $current_time_diff ? true : false;

        $resp = [
            'code'    => 200,
            'site_id' => $site_id,
            'station' => [
                'updated_at' => $data['updated_at'],
                'status'     => $is_online ? 'online' : 'offline'
            ]
        ];

        return Response::create($resp);
    }
}
