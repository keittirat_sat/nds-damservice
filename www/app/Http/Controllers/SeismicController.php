<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input,
    Illuminate\Support\Facades\Validator,
    Illuminate\Http\Response,
    App\Repositories\SeismicRepository,
    App\Repositories\SecondaryHost,
    App\Model\SyncControl,
    App\Repositories\TimeRepository,
    Illuminate\Support\Facades\DB,
    App\Libraries\QueueDataAdapter,
    App\Libraries\DataMonitor,
    App\Libraries\NdsDB,
    Illuminate\Http\Request,
    App\Libraries\RespClient;

/**
 * Description of SeismicController
 *
 * @author keitt
 */
class SeismicController extends Controller
{

    private $seismic_repo;
    private $time_repo;
    private $data_convert;
    private $nds_db;
    private $request;
    private $client;

    public function __construct(Request $request)
    {
        $this->seismic_repo = new SeismicRepository();
        $this->time_repo    = new TimeRepository();
        $this->data_convert = new QueueDataAdapter();
        $this->nds_db       = new NdsDB();
        $this->request      = $request;
        $this->client       = new RespClient();
    }

    public function getIndex()
    {
        $site_id = Input::get('site_id', null);
        $station = Input::get('station', null);
        $time    = Input::get('time', date('Y-m-d H:i:s'));

        if (empty($site_id) || empty($station)) {
            $resp = [
                'code'    => 500,
                'message' => 'Unauthorize access'
            ];

            return Response::create($resp, 500);
        }

        $station       = strtoupper($station);
        $time_range    = $this->time_repo->quarterTime($time);
        $db_collection = date('Ymd', $time_range['start_unix_time']) . '_' . $site_id;

        $data         = [];
        $data_reorder = [];

        if (config('app.read_from_file')) {
            /**
             * Get Data From File
             */
            $collected_data = $this->nds_db->readSeismic($site_id, $station, $time_range['start_unix_time'], $time_range['end_unix_time']);
        } else {
            /**
             * Get Data From database
             */
            $collected_data = DB::connection('mongodb')
                ->collection($db_collection)
                ->where('station', 'regex', "/{$station}/i")
                ->whereBetween('unix', [$time_range['start_unix_time'], $time_range['end_unix_time']])
                ->orderBy('unix', 'asc')
                ->get();
        }
        /**
         * Default Data
         */
        $resp = array(
            'code'   => 200,
            'result' => array(
                'location'     => array(
                    'site_id' => $site_id,
                    'station' => $station
                ),
                'time_zone'    => config('app.timezone'),
                'request_time' => $time,
                'quarter'      => $time_range,
                'stream'       => array()
            )
        );

        /**
         * Transform Data
         */
        if (!empty($collected_data)) {
            foreach ($collected_data as $each_rec) {
                $data_reorder[$each_rec['unix']] = [
                    'ch0'        => $each_rec['ch0'],
                    'ch1'        => $each_rec['ch1'],
                    'ch2'        => $each_rec['ch2'],
                    'ch0_max'    => isset($each_rec['ch0_max']) ? $each_rec['ch0_max'] : max(array_filter($each_rec['ch0'], 'abs')),
                    'ch1_max'    => isset($each_rec['ch1_max']) ? $each_rec['ch1_max'] : max(array_filter($each_rec['ch1'], 'abs')),
                    'ch2_max'    => isset($each_rec['ch2_max']) ? $each_rec['ch2_max'] : max(array_filter($each_rec['ch2'], 'abs')),
                    'time_stamp' => date('Y-m-d H:i:s', $each_rec['unix'])
                ];
            }

            /**
             * Check Missing Data, replace zero
             */
            for ($unix = $time_range['start_unix_time']; $unix <= $time_range['end_unix_time']; $unix++) {
                if (empty($data_reorder[$unix])) {
                    $data[$unix] = [
                        'ch0'        => [0, 0],
                        'ch1'        => [0, 0],
                        'ch2'        => [0, 0],
                        'ch0_max'    => 0,
                        'ch1_max'    => 0,
                        'ch2_max'    => 0,
                        'time_stamp' => date('Y-m-d H:i:s', $unix)
                    ];
                } else {
                    $data[$unix] = $data_reorder[$unix];
                }
            }

            $resp['result']['stream'] = $data;
        } else {
            /**
             * Get Data from secondary server
             */
            $is_master = config('archive.is_master');
            if ($is_master == true) {
                $path      = 'seismic';
                $all_input = Input::all();
                $raws_json = SecondaryHost::callBackupData($path, $all_input);
                if ($raws_json !== false) {
                    $resp = json_decode($raws_json, true);
                }
            }
        }

        return Response::create($resp);
    }

    public function getSync()
    {
        $site_id = Input::get('site_id', null);

        if (empty($site_id)) {
            return Response::create([
                    'code'     => 500,
                    'messages' => 'Unauthorize Access'
                    ], 500);
        }

        if (config('app.read_from_file')) {
            $data     = [];
            $ref_data = $this->nds_db->readSyncControl();
            foreach ($ref_data as $each_rec) {
                if ($each_rec['site_id'] == $site_id) {
                    $data[] = $each_rec;
                }
            }
        } else {
            $data = SyncControl::where('site_id', '=', $site_id)->get()->toArray();
        }

        if (empty($data)) {
            $resp = array(
                'code'         => 403,
                'sync_at'      => date('Y-m-d H:i:s'),
                'save_at'      => ['date' => date('Y-m-d H:i:s')],
                'station_info' => array()
            );
            return Response::create($resp);
        }

        $sensor = [];

        $sync_at = 0;
        $save_at = 0;

        foreach ($data as $each_sensor) {
            $sensor[] = array(
                'id'      => strtotime($each_sensor['latest_record_time']),
                'station' => $each_sensor['station'],
                'sync_at' => $each_sensor['latest_record_time'],
                'save_at' => ['date' => $each_sensor['updated_at']]
            );

            if ($sync_at < strtotime($each_sensor['latest_record_time'])) {
                $sync_at = strtotime($each_sensor['latest_record_time']);
                $save_at = $each_sensor['updated_at'];
            }
        }

        $resp = array(
            'code'         => 200,
            'id'           => $sync_at,
            'sync_at'      => date('Y-m-d H:i:s', $sync_at),
            'save_at'      => ['date' => $save_at],
            'station_info' => $sensor
        );

        return Response::create($resp);
    }

    public function postSync()
    {
        $site_id              = Input::get('site_id', null);
        $data                 = Input::get('data', null);
        $encode               = Input::get('encode', 0);
        $disable_sync_control = Input::get('disable_sync_control', 0);
        $preLog               = null;

        $validate = Validator::make(['site_id' => $site_id, 'data' => $data], ['site_id' => 'required', 'data' => 'required']);
        if (!is_null($data) && $validate->passes()) {
            /**
             * Decode with GZip
             */
            if ($encode != 0) {
                $data = rawurldecode($data);
                $data = base64_decode($data);
                $data = gzdecode($data);
            }

            $stream       = json_decode($data, true);
            $preLog       = $this->seismic_repo->convertRawsSeismic($site_id, $stream);
            $data_convert = $this->seismic_repo->convertToMongo($site_id, $preLog);
            $data_seismic = $this->data_convert->seismicConverter($data_convert['data']);

            /**
             * Monitoring
             */
            $data_monitor = new DataMonitor($site_id);
            $notice_data  = $data_monitor->seismicWave($data_seismic);

            if (!empty($notice_data['warning_level'])) {
                /**
                 * Push data warning into queue
                 * - first priority
                 */
                $notice_data['site_id'] = $site_id;

                $job = new \App\Jobs\SaveEvent($notice_data);
                $this->dispatch($job);
            }

            $chunk = array_chunk($data_seismic, 150);
            foreach ($chunk as $data_set) {
                $enqueue['site_id']              = $site_id;
                $enqueue['timezone']             = 'Asia/Bangkok';
                $enqueue['data']                 = $data_set;
                $enqueue['disable_sync_control'] = $disable_sync_control;
                /**
                 * Push all data into queue
                 */
                $job                             = new \App\Jobs\SaveSeismic($enqueue);
                $this->dispatch($job);
            }
        }

        /**
         * Push Raws
         */
        $push_list = config('pushlist.raws.seismic');
        if (!empty($push_list)) {
            foreach ($push_list as $service_url) {
                try {
                    $this->client->postJson($service_url, Input::all(), true);
                } catch (Exception $e) {
                    
                }
            }
        }

        return Response::create([
                'code'   => 200,
                'synced' => count($stream),
                'fwd'    => $push_list
        ]);
    }

    public function getEventArchive()
    {
        $site_id = Input::get('site_id', NULL);
        $limit   = (int) Input::get('limit', 20);
        $page    = Input::get('page', 1);

        if (config('app.read_from_file')) {
            $data = $this->nds_db->readSeismicEvent($site_id, $page, $limit);
        } else {
            $data = DB::connection('mongodb')->collection('event_' . $site_id)->orderBy('unix', 'desc')->paginate($limit)->toArray();
        }

        $resp_data = [];

        /**
         * Convert data to old format
         */
        foreach ($data['data'] as $each_rec) {
            /**
             * Find max data
             */
            $temp = [
                'timestamp' => date('Y-m-d H:i:s', $each_rec['unix']),
                'g'         => 0,
                'alert'     => false
            ];

            foreach ($each_rec['data'] as $data_station) {
                if ($data_station['g'] > $temp['g']) {
                    $temp['g']     = $data_station['g'];
                    $temp['alert'] = $data_station['alert'];
                }
            }

            $resp_data[] = $temp;
        }

        $resp = [
            'code'   => 200,
            'result' => [
                'logging_info' => [
                    'log'   => config('sensor.' . $site_id . '.max_seismic'),
                    'alarm' => config('sensor.' . $site_id . '.alert_seismic')
                ],
                'page'         => [
                    'current_page' => $data['current_page'],
                    'total_page'   => ceil($data['total'] / $limit)
                ],
                'info'         => [
                    'current_data' => $data['per_page'],
                    'total_data'   => $data['total']
                ],
                'data'         => $resp_data
            ]
        ];

        return Response::create($resp);
    }

    public function getStatus()
    {
        $site_id = Input::get('site_id', null);

        if (empty($site_id)) {
            $resp = [
                'code'    => 500,
                'message' => 'Unauthorize access'
            ];

            return Response::create($resp, 500);
        }

        if (config('app.read_from_file')) {
            $data     = [];
            $ref_data = $this->nds_db->readSyncControl();
            foreach ($ref_data as $each_rec) {
                if ($each_rec['site_id'] == $site_id) {
                    $data[] = $each_rec;
                }
            }
        } else {
            $data = SyncControl::where('site_id', '=', $site_id)->get()->toArray();
        }

        if (empty($data)) {
            $resp = [
                'code'    => 500,
                'message' => 'Unauthorize access #2'
            ];

            return Response::create($resp, 500);
        }

        $current_unix      = time();
        $maximum_time_diff = config('sensor/' . $site_id . '.no_update_interval');

        $resp = [
            'code'    => 200,
            'site_id' => $site_id,
            'station' => []
        ];

        foreach ($data as $each_rec) {
            $updated_unix      = strtotime($each_rec['updated_at']);
            $current_time_diff = $current_unix - $updated_unix;
            $is_online         = $maximum_time_diff <= $current_time_diff ? true : false;
            $resp['station'][] = [
                'station'            => $each_rec['station'],
                'updated_at'         => $each_rec['updated_at'],
                'latest_record_time' => $each_rec['latest_record_time'],
                'status'             => $is_online ? 'online' : 'offline'
            ];
        }

        return Response::create($resp);
    }
}
