<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

/**
 * Description of TimeRepository
 *
 * @author keitt
 */
class TimeRepository
{

    public function __construct()
    {
        
    }

    /**
     * 
     * @param date $time
     * @return array(start_time, end_time)
     */
    public function quarterTime($time)
    {
        $unix        = strtotime($time);
        $start_time  = ';
        $end_time    = ';
        $current_min = (int) date('i', $unix);

        if (($current_min >= 0) && ($current_min < 15)) {
            /**
             * Quarter 1
             */
            $start_time = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':00:00';
            $end_time   = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':14:59';
        } else if (($current_min >= 15) && ($current_min < 30)) {
            /**
             * Quarter 2
             */
            $start_time = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':15:00';
            $end_time   = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':29:59';
        } else if (($current_min >= 30) && ($current_min < 45)) {
            /**
             * Quarter 3
             */
            $start_time = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':30:00';
            $end_time   = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':44:59';
        } else if (($current_min >= 45) && ($current_min < 60)) {
            /**
             * Quarter 4
             */
            $start_time = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':45:00';
            $end_time   = date('Y-m-d', $unix) . ' ' . date('H', $unix) . ':59:59';
        }

        return [
            'start_time'      => $start_time,
            'end_time'        => $end_time,
            'start_unix_time' => strtotime($start_time),
            'end_unix_time'   => strtotime($end_time)
        ];
    }

    public function totalDay($start_date, $end_date)
    {
        $start      = strtotime($start_date);
        $end        = strtotime($end_date);
        $total_date = [];
        $date_diff  = ceil(($end - $start) / 86400);

        $i = 0;
        do {
            $unix_concern = ($i * 86400) + $start;
            $total_date[] = date('Y-m-d', $unix_concern) . ' 00:00:00';
            $i++;
        } while ($i < $date_diff);
        return $total_date;
    }
}
