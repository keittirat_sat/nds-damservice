<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Libraries\RespClient,
    Illuminate\Support\Facades\Validator;

/**
 * Description of AlertRepository
 *
 * @author keitt
 */
class AlertRepository
{

    private $client;
    private $email_url;
    private $line_url;
    private $email_pkt_rule;
    private $sms_pkt_rule;
    private $line_pkt_rule;
    private $line_direct_img_pkt_rule;

    public function __construct()
    {
        if (config('archive.is_master') === true) {
            $this->client          = new RespClient;
            $this->email_url       = config('url.base_url.alert_email');
            $this->sms_url         = config('url.base_url.alert_sms');
            $this->line_url        = config('url.base_url.alert_line');
            $this->line_direct_url = config('url.base_url.alert_direct_line');

            $this->email_pkt_rule = [
                "recv_email" => "required",
                "recv_name"  => 'required',
                "from_name"  => "required",
                "from_email" => "required",
                "subject"    => "required",
                "html"       => "required",
                "ch"         => "required"
            ];

            $this->sms_pkt_rule = [
                "tel"     => "required",
                "content" => "required",
                "app"     => "required"
            ];

            $this->line_pkt_rule = [
                "message" => "required"
            ];

            $this->line_direct_img_pkt_rule = [
                "mid" => "required",
                "img" => "required"
            ];
        }
    }

    /**
     * NDS Email Api
     * @param string $subject mail subject
     * @param string $html htmk in string form
     * @param string $recv_email email list seperate with comma(,)
     * @param string $recv_name name of recieve
     * @param string $ch
     * @param string $from_name (optional)
     * @param string $from_email (optional)
     * @return boolean
     */
    public function sendEmail($subject, $html, $recv_email, $recv_name, $ch, $from_name = 'Warning system', $from_email = "trycatch.service@gmail.com")
    {
        if (config('archive.is_master') === true) {
            $from_name .= " (site: {$ch})";
            $html     = (string) $html;
            /**
             * Pkt - mapping
             */
            $send_pkt = [
                "subject"    => $subject,
                "html"       => $html,
                "recv_name"  => $recv_name,
                "recv_email" => $recv_email,
                "from_name"  => $from_name,
                "from_email" => $from_email,
                "ch"         => $ch
            ];

            $validate = Validator::make($send_pkt, $this->email_pkt_rule);
            if ($validate->passes()) {
                try {
                    $raws_resp = $this->client->postJson($this->email_url, $send_pkt);
                    $resp_json = json_decode($raws_resp);
                    return ($resp_json->code == 200) ? true : false;
                } catch (Exception $e) {
                    /*
                     * Do noting
                     */
                }
            }
            return false;
        }

        return true;
    }

    /**
     * NDS SMS Api
     * @param string $tel_all Phone number seperate with comma
     * @param string $content raws text
     * @param string $app
     * @return boolean
     */
    public function sendSms($tel_all, $content, $app)
    {
        if (config('archive.is_master') === true) {
            $send_pkt = [
                "tel"     => $tel_all,
                "content" => trim((string) $content),
                "app"     => $app
            ];

            $validate = Validator::make($send_pkt, $this->sms_pkt_rule);
            if ($validate->passes()) {

                try {
                    $raws_resp = $this->client->postJson($this->sms_url, $send_pkt);
                    $resp_json = json_decode($raws_resp);
                    return ($resp_json->code == 200) ? true : false;
                } catch (Exception $e) {
                    /**
                     * Do noting
                     */
                }
            }

            return false;
        }

        return true;
    }

    public function sendLine($content, $app = 'rids')
    {
        if (config('archive.is_master') === true) {
            $send_pkt = [
                "message" => (string) $content
            ];

            $validate = Validator::make($send_pkt, $this->line_pkt_rule);
            if ($validate->passes()) {

                try {
                    $raws_resp = $this->client->postJson($this->line_url, $send_pkt);
                    $resp_json = json_decode($raws_resp);
                    return ($resp_json->code == 200) ? true : false;
                } catch (Exception $e) {
                    /**
                     * Do noting
                     */
                }
            }
            return false;
        }

        return true;
    }

    public function sendLineWithImg($content, $img = [], $app = 'rids')
    {
        if (config('archive.is_master') === true) {
            $send_pkt = [
                "text" => (string) $content,
                "img"  => $img,
                "ch"   => $app
            ];

            $validate = Validator::make($send_pkt, $this->line_pkt_rule);
            if ($validate->passes()) {

                $raws_resp = $this->client->postJson($this->line_url, $send_pkt);

                try {
                    $resp_json = json_decode($raws_resp);
                    return ($resp_json->code == 200) ? true : false;
                } catch (Exception $e) {
                    /**
                     * Do noting
                     */
                }
            }
            return false;
        }

        return true;
    }

    public function sendImgLineDirect($mid, $img)
    {
        if (config('archive.is_master') === true) {
            $send_pkt = [
                "mid" => is_array($mid) ? $mid : [$mid],
                "img" => is_array($img) ? $img : [$img]
            ];

            $validate = Validator::make($send_pkt, $this->line_direct_img_pkt_rule);
            if ($validate->passes()) {

                $raws_resp = $this->client->postJson($this->line_direct_url, $send_pkt);

                try {
                    $resp_json = json_decode($raws_resp);
                    return ($resp_json->code == 200) ? true : false;
                } catch (Exception $e) {
                    /**
                     * Do noting
                     */
                }
            }
            return false;
        }

        return true;
    }
}
