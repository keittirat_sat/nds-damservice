<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Libraries\RespClient,
    App\Repositories\AlertRepository;

/**
 * Description of SeismicRepository
 *
 * @author keitt
 */
class SeismicRepository
{

    private $alert_repo;
    private $client;

    public function __construct()
    {
        $this->client       = new RespClient();
        $this->alert_repo   = new AlertRepository();
    }

    public function convertRawsSeismic($site_id, $raws_array_seismic)
    {
        $max_G      = 0;
        $flag_alert = false;
        $alert_time = 0;
        $time       = time();
        foreach ($raws_array_seismic as &$each_record) {
            $each_record                 = (array) $each_record;
            $each_record['id']           = (int) $each_record['id'];
            $each_record["time_create"]  = (float) $each_record["time_create"];
            $each_record["record_stamp"] = (int) $each_record["record_stamp"];
            $each_record["sync_at"]      = date('Y-m-d H:i:s', $each_record["time_create"]);
        }

        return $raws_array_seismic;
    }

    /**
     * 
     * @param string $site_id
     * @param array $data
     */
    public function convertToMongo($site_id, $data)
    {
        if (empty($data)) {
            return false;
        }

        $save_mongo['site_id'] = $site_id;
        $save_mongo['data']    = [];

        $ref_update = [];
        foreach ($data as $each_record) {
            $unix = $each_record["time_create"];

            $station = $each_record["station"];

            $ref_update[$station] = array(
                "record_stamp" => $each_record["record_stamp"],
                "ref_id"       => $each_record["id"],
                "sync_at"      => $each_record["sync_at"]
            );

            $save_mongo['data'][$unix][$station] = $each_record["g"];
        }
        
        return $save_mongo;
    }
}
