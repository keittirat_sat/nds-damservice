<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SecondaryHost
 *
 * @author keitt
 */

namespace App\Repositories;

class SecondaryHost
{

    static function callBackupData($path, $input = array())
    {
        $param            = array();
        $secondary_server = config('archive.secondary_server');
        $secondary_server .= '/' . $path;
        foreach ($input as $key => $val) {
            $param[] = $key . '=' . $val;
        }
        $secondary_server .= '?' . implode('&', $param);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $secondary_server,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return $response;
        }
    }
}
