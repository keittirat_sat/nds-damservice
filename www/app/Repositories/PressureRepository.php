<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Repositories\TimeRepository,
    App\Repositories\AlertRepository,
    App\Libraries\RespClient,
    App\Model\SyncControl,
    App\Libraries\QueueDataAdapter;

/**
 * Description of PressureRepository
 *
 * @author keitt
 */
class PressureRepository
{

    private $time_repo;
    private $alert_repo;
    private $client;
    private $data_convert;

    public function __construct()
    {
        $this->time_repo    = new TimeRepository();
        $this->alert_repo   = new AlertRepository();
        $this->client       = new RespClient();
        $this->data_convert = new QueueDataAdapter();
    }

    /**
     * writePressure($site_id, $data)
     * @param type $site_id
     * @param type $data
     * @return type
     */
    public function convertToMongo($site_id, $data)
    {
        if (empty($data)) {
            return false;
        }

        $save_mongo['site_id'] = $site_id;
        $save_mongo['data']    = [];

        foreach ($data as $unix => $each_record) {
            $save_mongo['data'][$unix] = $each_record;
        }

        return $save_mongo;
    }
}
