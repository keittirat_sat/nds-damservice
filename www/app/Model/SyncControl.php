<?php
/**
 * Created by PhpStorm.
 * User: keittirat.sat
 * Date: 7/25/2016 AD
 * Time: 2:14 PM
 */

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SyncControl extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'sync_control';

}
