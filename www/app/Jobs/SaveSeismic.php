<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use App\Libraries\RespClient;
use App\Libraries\NdsDB;

class SaveSeismic extends Job implements ShouldQueue
{

    use InteractsWithQueue,
        SerializesModels;

    private $data;
    private $client;
    private $nds_db;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data   = $data;
        $this->client = new RespClient();
        $this->nds_db = new NdsDB();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $table_name = date('Ymd', $this->data['data'][0]['unix']) . '_' . $this->data['site_id'];

        $update_station = [];
        foreach ($this->data['data'] as $rec) {
            $station = $rec['station'];
            if (empty($update_station[$station]) || ($update_station[$station] < $rec['unix'])) {
                $update_station[$station] = $rec['unix'];
            }
        }

        if (config('app.save_to_db')) {
            /**
             * MongoDB
             */
            $all_connection = config('database.connections');
            foreach ($all_connection as $conn => $info) {
                if (preg_match('/mongodb_write(.*)/', $conn)) {
                    try {
                        /**
                         * Save chunk of data
                         */
                        DB::connection($conn)->collection($table_name)->insert($this->data['data']);

                        /**
                         * Update Sync Control
                         */
                        if (empty($this->data['disable_sync_control']) || $this->data['disable_sync_control'] == 0) {
                            foreach ($update_station as $station => $unix) {

                                DB::connection($conn)
                                    ->collection('sync_control')
                                    ->where('site_id', '=', $this->data['site_id'])
                                    ->where('station', '=', $station)
                                    ->update([
                                        'updated_at'         => date('Y-m-d H:i:s'),
                                        'latest_record_time' => date('Y-m-d H:i:s', $unix)
                                        ], ['upsert' => true]);
                            }
                        }
                        echo '[' . date('Y-m-d H:i:s') . '][SaveSeismic] insert into ' . $conn . PHP_EOL;
                    } catch (Exception $e) {
                        echo PHP_EOL . '[SaveSeismic] Connection Error: ' . $conn . PHP_EOL;
                    }
                }
            }
        }

        if (config('app.save_to_file')) {
            /**
             * File Database
             */
            foreach ($this->data['data'] as $each_rec) {
                $unix    = $each_rec['unix'];
                $station = $each_rec['station'];
                unset($each_rec['unix']);
                unset($each_rec['station']);
                $this->nds_db->saveSeismic($this->data['site_id'], $unix, $station, $each_rec);
            }

            if (empty($this->data['disable_sync_control']) || $this->data['disable_sync_control'] == 0) {
                foreach ($update_station as $station => $unix) {
                    $data     = $this->nds_db->readSyncControl();
                    $has_edit = false;
                    foreach ($data as &$rec) {
                        if ($rec['site_id'] == $this->data['site_id'] && $rec['station'] == $station) {
                            $has_edit = true;

                            $rec['updated_at']         = date('Y-m-d H:i:s');
                            $rec['latest_record_time'] = date('Y-m-d H:i:s', $unix);
                        }
                    }

                    if ($has_edit == false) {
                        $data[] = [
                            'site_id'            => $this->data['site_id'],
                            'station'            => $station,
                            'updated_at'         => date('Y-m-d H:i:s'),
                            'latest_record_time' => date('Y-m-d H:i:s', $unix)
                        ];
                    }

                    $this->nds_db->saveSyncControl($data);
                }
            }

            echo "Save File: " . $this->data['site_id'] . PHP_EOL;
        }

        /**
         * Push Data
         */
        $push_list = config('pushlist.seismic');
        if (!empty($push_list)) {
            foreach ($push_list as $service_url) {
                try {
                    echo 'Push: ' . $service_url . PHP_EOL;
                    $this->client->postJson($service_url, $this->data, true);
                } catch (Exception $e) {
                    echo 'Push-Error: ' . $service_url . PHP_EOL;
                }
            }
        } else {
            echo 'Push: Skip' . PHP_EOL;
        }

        echo '[' . date('Y-m-d H:i:s') . '][SaveSeismic]: ' . $this->data['site_id'] . PHP_EOL;
    }
}
