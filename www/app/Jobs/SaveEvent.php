<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB,
    App\Repositories\AlertRepository,
    App\Libraries\NdsDB,
    App\Libraries\RespClient;

class SaveEvent extends Job implements ShouldQueue
{

    use InteractsWithQueue,
        SerializesModels;

    private $data;
    private $config;
    private $alert_repo;
    private $nds_db;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data       = $data;
        $this->alert_repo = new AlertRepository();
        $this->config     = config('sensor.' . $data['site_id']);
        $this->nds_db     = new NdsDB();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $site_id       = $this->data['site_id'];
        $db_collection = 'event_' . $site_id;
        $time_limit    = 86400;
        $current_unix  = time();

        if (config('app.save_to_file')) {
            /**
             * File Database
             */
            foreach ($this->data['warning_level'] as $each_rec) {
                if (!empty($each_rec['unix']) && !empty($each_rec['data'])) {
                    $unix = $each_rec['unix'];
                    unset($each_rec['unix']);
                    $this->nds_db->saveSeismicEvent($site_id, $unix, $each_rec['data']);
                }
            }
        }

        if (config('app.save_to_db')) {
            /**
             * MongoDB
             */
            $all_connection = config('database.connections');
            foreach ($all_connection as $conn => $info) {
                if (preg_match('/mongodb_write(.*)/', $conn)) {
                    /**
                     * Save warning data into Database before normal data
                     */
                    DB::connection($conn)
                        ->collection($db_collection)
                        ->insert($this->data['warning_level']);

                    echo '[' . date('Y-m-d H:i:s') . '][SaveEvent] ' . $conn . PHP_EOL;
                }
            }
        }

        /**
         * Alert data
         */
        if (!empty($this->data['alert_level'])) {
            $allow_alert = $current_unix - $this->data['alert_level']['unix'] < $time_limit ? true : false;
            if ($allow_alert) {
                $all_station = config('sensor.' . $site_id . '.station');
                $web_link    = config('sensor.' . $site_id . '.website');
                $timestamp   = date('Y-m-d H:i:s', $this->data['alert_level']['unix']);
                $delimiter_name = explode('_', $site_id);

                /**
                 * SMS
                 */
                if ($this->config['alert_method']['sms'] == true) {
                    $sms_number  = $this->config['alert_sms_report'];
                    $sms_number  = implode(',', $sms_number);
                    $sms_content = (string) view('sms.alert.seismic', [
                            'full_name' => $this->config['site_name'],
                            'max_g'     => $this->data['alert_level']['g'],
                            'time'      => $timestamp,
                            'web_link'  => $web_link
                    ]);

                    $this->alert_repo->sendSms($sms_number, $sms_content, $this->config['sms_channel']);
                }

                /**
                 * Email
                 */
                $all_link = [];
                foreach ($all_station as $each_station) {
                    $all_link[] = $web_link . '/page/seismic/' . $each_station . '/' . rawurlencode($timestamp);
                }

                if ($this->config['alert_method']['email'] == true) {
                    $recv_name  = config('sensor.' . $site_id . '.recv_name');
                    $recv_email = config('sensor.' . $site_id . '.alert_email_report');
                    $recv_email = implode(',', $recv_email);
                    $subject    = '[' . $this->config['site_name'] . '] ' . $this->config['email_alert_subject'];
                    $data_temp  = [
                        'full_name' => $this->config['site_name'],
                        'max_g'     => $this->data['alert_level']['g'],
                        'time'      => $timestamp,
                        'all_link'  => $all_link
                    ];

                    $html = view('emails.alert.seismic', $data_temp);
                    $ch   = config('sensor.' . $site_id . '.email_channel');

                    $this->alert_repo->sendEmail($subject, $html, $recv_email, $recv_name, $ch);
                }

                /**
                 * Line
                 */
                 if ($this->config['alert_method']['line'] == true) {
                     $data_line = [
                         "full_name" => $this->config['site_name'],
                         "max_g"     => $this->data['alert_level']['g'],
                         "time"      => $timestamp,
                         "all_link"  => $all_link
                     ];
                     $app       = config("sensor.{$site_id}.sms_channel");
                     $content   = view('line.alert.seismic', $data_line);
                     $this->alert_repo->sendLine($content, $app);
                 }

                /**
                 * Seismic wave report
                 */
                if (config('app.seismic_wave_report', true) == true) {
                    try {
                        $client = new RespClient();
                        $payload = [
                            'datetime'          => $timestamp,
                            'site_id'           => $site_id,
                            'pressure_site_id'  => (config("sensor." . $delimiter_name[0] . '_pressure' . ".site_name", false) === false) ? null : $delimiter_name[0] . '_pressure',
                            'maxG'              => config("sensor.{$site_id}.max_accelerate", 0),
                            'station'           => config("sensor.{$site_id}.station", []),
                            'email'             => config("sensor.{$site_id}.alert_email_report", []),
                            'url'               => config('url.base_url.main_server') . '/seismic',
                            'pressure_url'      => config('url.base_url.main_server') . '/pressure'
                        ];
                        $client->postJson(config('url.base_url.effect_report'), $payload);
                    } catch(Exception $ex) {
                        print_r($ex);
                    }
                }

                /**
                 * Current Event Report
                 */
                if (config('app.event_report', false) == true) {
                    try {
                        $client         = new RespClient();
                        if (count($delimiter_name) > 0) {
                            $data_report = [
                                "site_id"  => $delimiter_name[0],
                                "type"     => ["pressure", "seismic"],
                                "time"     => $timestamp,
                                "callback" => config('url.base_url.event_export_callback')
                            ];
                            $client->postJson(config('url.base_url.event_export'), $data_report);
                        }
                    } catch(Exception $ex) {
                        print_r($ex);
                    }
                }

                echo '[' . date('Y-m-d H:i:s') . '][SaveEvent] Alert Detect Record time: ' . date('Y-m-d H:i:s', $this->data['alert_level']['unix']) . PHP_EOL;
            } else {
                echo '[' . date('Y-m-d H:i:s') . '][SaveEvent] Alert Skip Record time: ' . date('Y-m-d H:i:s', $this->data['alert_level']['unix']) . PHP_EOL;
            }
        }

        echo '[' . date('Y-m-d H:i:s') . '][SaveEvent] Synced: ' . $site_id . PHP_EOL;
    }
}
