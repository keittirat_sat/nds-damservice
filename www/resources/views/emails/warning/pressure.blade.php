<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php $time = isset($time) ? $time : date("Y-m-d H:i:s"); ?>
        <h1>ระบบรายงานการเชื่อมต่อเครื่องมือวัดแรงดันน้ำภายในเขื่อนของ <b><?php echo $full_name ?></b></h1>
        <p>ไม่สามารถติดต่อกับระบบเขื่อนได้ โดยข้อมูลที่เข้ามาครั้งสุดท้ายล่าสุดเมื่อ <?php echo $last_updated; ?> โดยเป็นข้อมูลของเวลาที่ <?php echo $last_record_time; ?></p>
        <p style="font-size: ">Report Generated by TryCatch | NDS @ <?php echo $time; ?></p>
    </body>
</html>
