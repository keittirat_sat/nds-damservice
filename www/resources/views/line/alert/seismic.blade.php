แจ้งเตือนข้อมูลเสี่ยงต่อการเกิดแผ่นดินไหว (<?php echo $full_name; ?>)

ระบบตรวจพบข้อมูลวัดได้สูงสุดที่ <?php echo $max_g; ?>G

ที่เวลา <?php echo $time . PHP_EOL; ?>

รายละเอียดสามารถเข้าไปดูได้ที่
<?php foreach ($all_link as $each_link): ?>
<?php echo PHP_EOL . $each_link . PHP_EOL; ?>
<?php endforeach; ?>