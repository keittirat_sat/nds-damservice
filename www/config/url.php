<?php
return [
    'base_url' => [
        'alert'                 => 'http://q.cloudnds.com',
        'service'               => 'http://q.cmrabbit.com',
        'q'                     => 'http://q.cloudnds.com',
        'alert_email'           => 'http://api.cmrabbit.com/email/send',
        'alert_sms'             => 'http://api.cmrabbit.com/sms/send',
        'alert_line'            => 'http://api.cmrabbit.com/line/send',
        'alert_direct_line'     => 'http://api.cmrabbit.com/line/direct',
        'event_export'          => 'http://export.cloudnds.com/export',
        'event_export_callback' => 'http://api.cloudnds.com/report/current-event',
        'effect_report'         => 'http://effect.secondary.damlog.com/report',
        'main_server'           => 'http://api.cloudnds.com'
    ]
];
