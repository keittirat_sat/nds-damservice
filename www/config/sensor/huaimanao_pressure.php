<?php
return array(
    'site_name'             => 'อ่างเก็บน้ำห้วยแม่มะนาว',
    'recv_name'             => 'IRR-Huaimanao Officer',
    'alert_email_report'    => array(
        "trycatch.service@gmail.com",
        "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'error_report'          => array(
        "trycatch.service@gmail.com",
        # "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "abcdef255@gmail.com"
    ),
    'website'               => 'http://huaimanao.damlog.com',
    # 'no_update_interval'    => 1800,
    'no_update_interval'    => 31536000,
    'email_alert_subject'   => '',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแรงดันน้ำภายในเขื่อน',
    'email_channel'         => 'irr-huaimanao'
);
