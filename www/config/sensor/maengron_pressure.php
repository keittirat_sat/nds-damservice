<?php
return array(
    "site_name"             => "ลุ่มน้ำแม่งอน",
    'recv_name'             => 'IRR-Maengron Officer',
    'alert_email_report'    => array(
        'trycatch.service@gmail.com',
        'noowww4488@gmail.com',
        'chatpongnoi@hotmail.com'
    ),
    'error_report'          => array(
        "trycatch.service@gmail.com",
        "chatpongnoi@hotmail.com",
        "pavanon.s@multiproof.com",
        "suttichai@mtp.co.th",
        'noowww4488@gmail.com'
    ),
    'website'               => 'http://maengron.damlog.com',
    'no_update_interval'    => 1800,
    'email_alert_subject'   => '',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัด',
    'email_channel'         => 'irr-maengron'
);
