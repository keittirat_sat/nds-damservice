<?php
return array(
    "site_name"             => "อ่างเก็บน้ำแม่ฮ่องสอน",
    'max_seismic'           => 0.001,
    'alert_seismic'         => 0.0025,
    'max_accelerate'        => 0.247,
    'recv_name'             => 'IRR-Maehongson Officer',
    'monitor_station'       => '4019',
    'alert_method'          => array(
        'sms'   => false,
        'email' => true,
        'line'  => true
    ),
    'alert_sms_report'      => array(
        "0869120099",
        "0954492332",
        "0819517050",
        # "0804915959",
        "0818856488"
    ),
    'alert_email_report'    => array(
        "trycatch.service@gmail.com",
        "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'error_report'          => array(
        "trycatch.service@gmail.com",
        # "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "abcdef255@gmail.com"
    ),
    'website'               => 'http://maehongson.damlog.com',
    'accelerometer'         => array('4017', '4018', '4019'),
    'station'               => array('4017', '4018', '4019'),
//    'no_update_interval'    => 900,
    'no_update_interval'    => 8640000,
    'email_alert_subject'   => 'พบแรงสั่นสะเทือนมากกว่าค่าที่กำหนด',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแผ่นดินไหว',
    'email_channel'         => 'irr-maehongson',
    'sms_channel'           => 'irr-maehongson'
);
