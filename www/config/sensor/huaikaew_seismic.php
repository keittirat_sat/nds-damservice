<?php
return array(
    "site_name"             => "อ่างเก็บน้ำห้วยแก้ว",
    'max_seismic'           => 0.001,
    'alert_seismic'         => 0.0025,
    'max_accelerate'        => 0.247,
    'recv_name'             => 'IRR-Huaikaew Officer',
    'monitor_station'       => '4020',
    'alert_method'          => array(
        'sms'   => false,
        'email' => true,
        'line'  => true
    ),
    'alert_sms_report'      => array(
        "0869120099",
        "0954492332",
        "0819517050",
        # "0804915959",
        "0818856488"
    ),
    'alert_email_report'    => array(
        "trycatch.service@gmail.com",
        "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'error_report'          => array(
        "trycatch.service@gmail.com",
        # "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "abcdef255@gmail.com"
    ),
    'website'               => 'http://huaikaew.damlog.com',
    'accelerometer'         => array('4020'),
    'station'               => array('4020'),
    'no_update_interval'    => 900,
    'email_alert_subject'   => 'พบแรงสั่นสะเทือนมากกว่าค่าที่กำหนด',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแผ่นดินไหว',
    'email_channel'         => 'irr-huaikaew',
    'sms_channel'           => 'irr-huaikaew'
);
