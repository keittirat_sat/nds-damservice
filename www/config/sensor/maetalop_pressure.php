<?php
return array(
    "site_name"             => "อ่างเก็บน้ำแม่ทะลบ",
    'recv_name'             => 'IRR-Maetalop Officer',
    'alert_email_report'    => array(
        "trycatch.service@gmail.com",
        "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'error_report'          => array(
        "trycatch.service@gmail.com",
        # "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "abcdef255@gmail.com"
    ),
    'website'               => 'http://maetalop.damlog.com',
    'no_update_interval'    => 1800,
    'email_alert_subject'   => '',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแรงดันน้ำภายในเขื่อน',
    'email_channel'         => 'irr-maetalop'
);
