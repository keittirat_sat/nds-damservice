<?php
return array(
    "site_name"             => "อ่างเก็บน้ำแม่ทะลบ",
    'max_seismic'           => 0.0001,
    'alert_seismic'         => 0.001,
    'max_accelerate'        => 0.247,
    'recv_name'             => 'IRR-Maetalop Officer',
    'monitor_station'       => '5he4',
    'alert_method'          => array(
        'sms'   => false,
        'email' => true,
        'line'  => true
    ),
    'alert_sms_report'      => array(
        "0869120099",
        "0954492332",
        "0819517050",
        # "0804915959",
        "0818856488"
    ),
    'alert_email_report'    => array(
        "trycatch.service@gmail.com",
        "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'error_report'          => array(
        "trycatch.service@gmail.com",
        # "lert.januwat@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "abcdef255@gmail.com"
    ),
    'website'               => 'http://maetalop.damlog.com',
    'accelerometer'         => array('5he2', '5he3', '5he4'),
    'station'               => array('5he2', '5he3', '5he4'),
    'no_update_interval'    => 900,
    'email_alert_subject'   => 'พบแรงสั่นสะเทือนมากกว่าค่าที่กำหนด',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแผ่นดินไหว',
    'email_channel'         => 'irr-maetalop',
    'sms_channel'           => 'irr-maetalop'
);
