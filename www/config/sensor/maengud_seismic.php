<?php
return array(
    'site_name'             => 'เขื่อนแม่งัด',
//    'site_name'             => 'เขื่อนแม่งัดสมบูรณ์ชล',
    'count_to_G'            => 1.1511900000000000E-06,
    'count_to_gravity'      => 6.4390311254547700E-05,
    'count_to_accelerate'   => 6.57043992393344E-06,
    'max_seismic'           => 0.001235,
    'alert_seismic'         => 0.00247,
    'max_accelerate'        => 0.247,
    'recv_name'             => 'IRR-Maengud Officer',
    'monitor_station'       => 'BBC5',
    'alert_method'          => array(
        'sms'   => true,
        'email' => true,
        'line'  => true
    ),
    'alert_sms_report'      => array(
        "0869120099",
        "0954492332",
        # "0861488820",
        "0819517050",
        # "0804915959",
        "0818856488"
    ),
    'alert_email_report'    => array(
        "dampinstruments@gmail.com",
        "lert.januwat@gmail.com",
        "trycatch.service@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "sirikanyalaosuwan@gmail.com",
        "pearasynp@gmail.com"
    ),
    'error_report'          => array(
        "dampinstruments@gmail.com",
        # "lert.januwat@gmail.com",
        "trycatch.service@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'website'               => 'http://maengud.damlog.com',
    'accelerometer'         => array('BA29', 'B87C', 'B887'),
    'station'               => array('BBC5', 'BA29', 'B87C', 'B887'),
    'no_update_interval'    => 1800,
    'email_alert_subject'   => 'พบแรงสั่นสะเทือนมากกว่าค่าที่กำหนด',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแผ่นดินไหว',
    'email_channel'         => 'irr-maengud',
    'sms_channel'           => 'irr-maengud'
);
