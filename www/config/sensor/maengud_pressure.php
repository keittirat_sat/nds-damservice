<?php
return array(
    'site_name'             => 'เขื่อนแม่งัดสมบูรณ์ชล',
    'recv_name'             => 'IRR-Maengud Officer',
    'alert_email_report'    => array(
        "dampinstruments@gmail.com",
        "lert.januwat@gmail.com",
        "trycatch.service@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com",
        "sirikanyalaosuwan@gmail.com",
        "pearasynp@gmail.com"
    ),
    'error_report'          => array(
        "dampinstruments@gmail.com",
        # "lert.januwat@gmail.com",
        "trycatch.service@gmail.com",
        "chatpongnoi@hotmail.com",
        # "pirdo1cnx@gmail.com",
        "noowww4488@gmail.com"
    ),
    'website'               => 'http://maengud.damlog.com',
    'no_update_interval'    => 1800,
    'email_alert_subject'   => '',
    'email_warning_subject' => 'ไม่สามารถติดต่อกับระบบเครื่องมือวัดแรงดันน้ำภายในเขื่อน',
    'email_channel'         => 'irr-maengud'
);
