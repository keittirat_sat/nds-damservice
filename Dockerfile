FROM keittirat/nds-php7:alpine-mongo
RUN apk add --no-cache \
	python \
	py-pip

VOLUME /var/www/html

ADD https://getcomposer.org/download/1.6.5/composer.phar /usr/bin/composer.phar
RUN chmod +x /usr/bin/composer.phar

RUN pip install supervisor

ADD www /var/www/html/nds-damservice
COPY setup/app/master/archive.php /var/www/html/nds-damservice/config/archive.php
COPY setup/app/master/pushlist.php /var/www/html/nds-damservice/config/pushlist.php
RUN chmod 0777 -R /var/www/html/nds-damservice/storage

RUN mkdir /etc/supervisor

ADD start.sh /usr/bin/start_service.sh
RUN chmod +x /usr/bin/start_service.sh

ADD setup/supervisor/supervisord.conf /etc/supervisord.conf
ADD setup/supervisor/task /etc/supervisor/task

RUN cd /var/www/html/nds-damservice && composer.phar dumpautoload && php artisan optimize

EXPOSE 9000 9001

CMD ["sh","/usr/bin/start_service.sh"]